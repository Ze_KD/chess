// Global var
{
    var l;                          // the list of piece
    var ctx;                        // the canvas

    //var V0 = false;
    var player_color = 0;
    var selected_piece ;
    var is_end = false ;

    var show_capture = false;        // if indication of move are show
    var nb_move_black = [0,0,0];    // possiblity of move for black
    var nb_move_white = [0,0,0];    // possiblity of move for white
}

function start(event){
    //console.log('%cStart', 'background: #0000ff80; color: #bada55');
    ctx = document.getElementById('canvas').getContext('2d');
    
    //https://commons.wikimedia.org/wiki/File:Chess_Pieces_Sprite.svg

    l = [];
    for (let i = 0; i<8 ; i++){
        l.push(new piece('p',0,i,6));
        l.push(new piece('p',1,i,1));
    }
    {
        l.push(new piece('t',1,0,0));
        l.push(new piece('t',1,7,0));
        l.push(new piece('t',0,0,7));
        l.push(new piece('t',0,7,7));

        l.push(new piece('c',1,6,0));
        l.push(new piece('c',1,1,0));
        l.push(new piece('c',0,1,7));
        l.push(new piece('c',0,6,7));

        l.push(new piece('f',1,2,0));
        l.push(new piece('f',1,5,0));
        l.push(new piece('f',0,2,7));
        l.push(new piece('f',0,5,7));

        l.push(new piece('q',1,3,0));
        l.push(new piece('q',0,4,7));
        l.push(new piece('r',1,4,0));
        l.push(new piece('r',0,3,7));
    }


    draw()
    is_win()

    mouseMove(event)
    
}

function mouseMove(event){
    const x = Math.abs(Math.round(event.x/100-0.5));
    const y = Math.abs(Math.round(event.y/100-0.5));

    var doc = document.getElementById("donne" ).innerHTML = ''+x+' '+ y;
    var doc = document.getElementById("donne2").innerHTML = ''+event.x+' '+event.y;
    var doc = document.getElementById("donne3").innerHTML = 'V0 : '+0;
    var doc = document.getElementById("donne4").innerHTML = 'color : '+player_color;
    var doc = document.getElementById("donne5").innerHTML = 'piece : '+ l.findIndex(x => x==selected_piece);
    var doc = document.getElementById("donne6").innerHTML = 'nb black : '+nb_move_black;
    var doc = document.getElementById("donne7").innerHTML = 'nb white : '+nb_move_white;
}

function draw(){
    for (let i=0; i<8; i++){
        for (let j=0; j<8; j++){
            if ((i+j)%2 !=0){
                ctx.fillStyle = 'rgb(50,50,50)';
                ctx.fillRect(i*100,j*100,100,100)
            }
            else {
                ctx.fillStyle = 'rgb(150,150,150)';
                ctx.fillRect(i*100,j*100,100,100)
            }


            if (
                getPiece(i,j) != undefined && can_be_capture(i,j,getPiece(i,j).color)
                &&  show_capture
            ){
                ctx.fillStyle = 'rgb(0,0,200)'
                ctx.fillRect(i*100,j*100,10,10)
            }
            else if ( getPiece(i,j) == undefined && can_be_capture(i,j) &&  show_capture){
                ctx.fillStyle = 'rgb(255,100,100)'
                ctx.fillRect(i*100,j*100,10,10)
            }
        }
    }
    for (let i = 0; i<l.length; i++){
        l[i].drawImg()
    }
}

function key(event){
    if (event.key == 'Escape'){
        selected_piece = undefined
    }
}

function getPiece(x,y){
    for (let i = 0 ; i<l.length ; i++ ){
        if (l[i].x == x && l[i].y == y){
            return  l[i];
        }
    }
    return undefined
}

function clic(event){
    const x = Math.abs(Math.round(event.x/100-0.5));
    const y = Math.abs(Math.round(event.y/100-0.5));


    if ( ! is_end ){
        if ( x > 7){
            selected_piece = undefined
            draw()
        }
        
        else if (
            getPiece(x,y) != undefined &&
            getPiece(x,y).color == player_color
        ){
            clic1(x,y)
        }
        else if  (
            selected_piece != undefined
            && selected_piece.can_move(x,y)
            && !selected_piece.is_in_check(x,y)
            && selected_piece.color == player_color
        ) {
            clic2(x,y)
        }
        mouseMove(event)
    }
}

function clic1(x,y){
    draw()

    selected_piece = getPiece(x,y)
    selected_piece.toMove()
    
}

function clic2(x,y){
    

    var del = getPiece(x,y);

    // Castling 
    if (selected_piece.type == 'r' && selected_piece.castling(x,y)){
        console.log('castling')
        selected_piece.can_castle = false
        if ( x== 1 && y== 0 ){ getPiece(0,0).x += 2 }
        else if ( x== 1 && y== 7 ){ getPiece(0,7).x += 2 }
        else if ( x== 6 && y== 0 ){ getPiece(7,0).x -= 2 }
        else if ( x== 6 && y== 7 ){ getPiece(7,7).x -= 2 }

    }

    // Piece move
    selected_piece.x = x
    selected_piece.y = y

    // Pion --> Queen
    if ((selected_piece.y==7 || selected_piece.y==0) && selected_piece.type=="p"){
        selected_piece.type='q'
    }
    
    //Kill
    if (del != undefined){
        if (del.color == selected_piece.color){console.error('Team Kill'); }
        l = l.filter(x => x != del)
    }

    //Win
    if ( white_check() ){ console.log('white king check')}
    if ( black_check() ){ console.log('black king check')}

    is_win()
    if (nb_move_white[1] == 0){
        console.log('\n white king checkmate \n ')
        is_end = true
    }
    if (nb_move_black[1] == 0){
        console.log('\n black king checkmate \n ')
        is_end = true
    }


    draw()

    {
    player_color = 1 - player_color

    //V0 = false
    //selected_piece = undefined
    }
}

function can_be_capture(x,y,color){
    me = new piece('type',color,x,y)
    for ( let i = 0; i < 8; i++){
        for ( let j = 0; j < 8; j++){
            p = getPiece(i,j)
            if ( p != undefined && p.color != color ){
                type = p.type
                
                if (
                    ( me.pawns(i,j) && me.distance(i,j,2) && type == 'p' ) ||
                    ( me.rook(i,j) && (type == 't' || type == 'q') ) ||
                    ( me.diagonal(i,j) && (type == 'f' || type == 'q') ) ||
                    ( me.distance(i,j,5) && type == 'c' ) ||
                    ( me.distance(i,j,1) && type == 'r' ) ||
                    ( me.distance(i,j,2) && type == 'r' )
                ){return true}

            }
        }
    }
    return false    

}

function white_check(x = l[l.length-1].x, y = l[l.length-1].y){
    return can_be_capture(x,y , 0)
}
function black_check(x = l[l.length-2].x, y = l[l.length-2].y){
    return can_be_capture(x,y, 1)
}

function is_win(){

    nb_move_black = [0,0,0];
    nb_move_white = [0,0,0];

    for (var i = 0; i < l.length; i++){
        for (let x = 0; x<8; x++){
            for (let y = 0; y < 8; y++){
                if ( l[i].color == 0 ){
                    if ( l[i].can_move(x,y)  ){
                        nb_move_white[0] += 1
                        if ( ! l[i].is_in_check(x,y)){
                            nb_move_white[1] += 1
                            if ( l[i].is_in_check(x,y, 1-l[i].color) ){
                                nb_move_white[2] += 1
                            }
                        }
                    }
                }
                if ( l[i].color == 1 ){
                    if ( l[i].can_move(x,y)  ){
                        nb_move_black[0] += 1
                        if ( ! l[i].is_in_check(x,y)){
                            nb_move_black[1] += 1
                            if ( l[i].is_in_check(x,y, 1-l[i].color) ){
                                nb_move_black[2] += 1
                            }
                        }
                    }
                }
                
            }
        }
    }

}
