class piece {
    constructor(type,color,x,y){
        this.type = type;
        this.color = color;
        this.x = x;
        this.y = y;
        if (this.type == 'r'){ this.can_castle = true}
    }

    drawImg() {
        var sx=0, sy=0;
        if (this.color==1){sy=1}
    
        if (this.type=='k'){sx=0}
        if (this.type=='q'){sx=1}
        if (this.type=='f'){sx=2}
        if (this.type=='c'){sx=3}
        if (this.type=='t'){sx=4}
        if (this.type=='p'){sx=5}

        var img = new Image();
        
        var x=this.x ; var y = this.y;

        img.onload = function(){
            
            ctx.drawImage(img, sx*45, sy*45, 45, 45, 100*x+5, 100*y+5, 90, 90)
            ctx.stroke();
        }
        img.src = "main.png";
    }
    
    toMove(){
        for (let i = 0 ; i<8 ; i++ ){
            for (let j = 0 ; j<8 ; j++ ){
                if (this.can_move(i,j)) {
                    ctx.fillStyle = 'blue';
                    if ( this.is_in_check(i,j) ){
                        ctx.fillStyle = 'rgba(255,255,255,0)'; // brown
                    } else if ( this.is_in_check(i,j, 1-this.color ) ){
                        ctx.fillStyle = 'orange'
                    } else if (getPiece(i,j) != undefined){
                        ctx.fillStyle = 'green'
                    }
                    ctx.fillRect(i*100,j*100,100,100)
                }
            }
        }
        ctx.fillStyle = 'yellow';
        ctx.fillRect(this.x*100,this.y*100,100,100)
    }

    /** Chek if the move in x,y is correct */
    can_move(x,y,s=0){

        if (s==1 && this.type != 'r'){return 'ERROR'}
        if (x==this.x && y==this.y){return false}
        if (getPiece(x,y) != undefined && getPiece(x,y).color == this.color){
            return false
        }

        for (let i=0; i<l.length; i++){
            if (l[i].x == x && l[i].y==y && l[i].color==this.color){return false}
        }

        // pawns / pions
        if (this.type == 'p'){
            return this.pawns(x,y)
        }
        // rook / tour
        if (this.type == 't'){
            return this.rook(x,y)
        }
        // bishop // fou
        if (this.type == 'f'){
            return this.diagonal(x,y)
        }
        // knight / chevalier
        if (this.type == 'c'){
            return this.distance(x,y,5)
        }
        // queen / reine
        if (this.type == 'q'){
            return this.rook(x,y) || this.diagonal(x,y)
        }
        // king / roi
        if (this.type == 'r'){
            
            return (
                (
                    this.distance(x,y,1)
                    || this.distance(x,y,2)
                    || this.castling(x,y)
                ) 
                // && 
                // (
                //     ( ! white_check(x,y) && this.color==0) 
                //     || (black_check(x,y) && this.color==1 )
                // )

            )
        }

        

    }


    pawns(x,y){
        // check impossible move
        if ( x > this.x + 1 || x < this.x - 1 || y > this.y + 2 || y < this.y - 2){
            return false
        }

        // check if move in diagonal for capture ( getPiece )
        if ( getPiece(x,y) != undefined && x != this.x+1 && x != this.x-1 ){
            return false
        }
        // check if move forward ( good x cord )
        if ( getPiece(x,y) == undefined && x != this.x){
            return false
        }
        // check if move the good distance ( good y cord )
        if ( Math.abs(this.y-y) == 2 && this.x != x ){
            return false
        }
        if ( this.color==1 && this.y == 1 && y != this.y+2 && y != this.y+1 ){
            return false
        }
        if ( this.color==1 && this.y != 1 && y != this.y+1 ){
            return false
        }
        if ( this.color==0 && this.y == 6 && y != this.y-2 && y != this.y-1 ){
            return false
        }
        if ( this.color==0 && this.y != 6 && y != this.y-1 ){
            return false
        }
        return true
    }
    
    rook(x,y){
        
        // Chek if not move in diagonal
        if ( x!=this.x && y!=this.y ){return false}

        else if (x == this.x){
            // check if road is empty
            for (let i = y ; i != this.y ; i+= 2 * ( this.y > y ) - 1 ){
                if (getPiece(x,i) != undefined){
                    if ( getPiece(x,i).type == this.type ){return false}
                    else if ( i!= y ){return false}
                }
                if (i>10 || i<-10){console.log('brk'); return false}
            }
            return true    
        }

        else if (y == this.y){
            // check if road is empty
            for (let i = x ; i != this.x ; i+= 2 * ( this.x > x ) - 1 ){
                if (getPiece(i,y) != undefined){
                    if ( getPiece(i,y).type == this.type ){return false}
                    else if ( i!= x ){return false}
                }
                if (i>10 || i<-10){console.log('brk'); return false}
            }
            return true    
        }
    }
        
    diagonal(x,y){
            if (Math.abs(this.x - x) != Math.abs(this.y - y )){ return false }
            if ( this.x == x && this.y == y ){ return false }

            for (let i = 0 ; i != Math.abs(this.x-x) ; i+= 1 ){
                let kx = (this.x > x) * 2 - 1, ky = (this.y > y) * 2 - 1 
                if (getPiece(x+i*kx,y+i*ky) != undefined){
                    if ( getPiece(x+i*kx,y+i*ky).type == this.type ){return false}
                    else if ( i != 0 ){ return false }
                }
                if (i>10 || i<-10){console.log('brk'); return false}
            }
            return true 
            
    }

    distance(x,y,n){
        return (x-this.x)**2 + (y-this.y)**2 == n
    }

    castling(x,y){
        if ( ! this.can_castle ){return false}
        if ( this.y != y ){return false}


        if (this.color == 0){
            if ( this.x != 3 || this.y != 7 ){ return false }
            if (
                x == 6 && getPiece(7,7) != undefined && getPiece(7,7).type == 't'
                && getPiece(6,7) == undefined && getPiece(5,7)== undefined && getPiece(4,7) == undefined
            ){
                return true
            }
            if (
                x == 1 && getPiece(0,7).type == 't'
                && getPiece(1,7) == undefined && getPiece(2,7) == undefined
            ){
                return true
            }
            return false



        }
        else if (this.color ==1){
            if ( this.x!=4 && this.y!=0 ){ return false }
            if (
                x == 6 && getPiece(7,7).type == 't'
                && getPiece(6,0) == undefined && getPiece(5,0)== undefined && getPiece(4,0) == undefined
            ){
                return true
            }
            if (
                x == 1 && getPiece(0,7).type == 't'
                && getPiece(1,0) == undefined && getPiece(2,0) == undefined
            ){
                return true
            }
            return false




        }


    }

    is_in_check(x,y, color = this.color){
        let oldx = this.x, oldy = this.y 
        this.x = x, this.y = y
        let r = false

        if (color == 0 && white_check()){r = true}
        if (color == 1 && black_check()){r = true}

        this.x = oldx, this.y = oldy
        return r
    }
    
}
